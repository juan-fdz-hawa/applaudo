from rest_framework_extensions.routers import ExtendedSimpleRouter
from .views import LikeViewSet, MovieViewSet, SaleViewSet, RentalViewSet

router = ExtendedSimpleRouter()

movies_routes = router.register(
    "",
    MovieViewSet,
    basename="movie",
)

movies_routes.register(
    "sales",
    SaleViewSet,
    basename="movie-sale",
    parents_query_lookups=["movie"],
)

movies_routes.register(
    "rentals",
    RentalViewSet,
    basename="movie-rental",
    parents_query_lookups=["movie"],
)

movies_routes.register(
    "likes",
    LikeViewSet,
    basename="movie-like",
    parents_query_lookups=["movie"],
)

urlpatterns = router.urls
