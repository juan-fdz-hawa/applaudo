import factory
from factory.django import DjangoModelFactory
from movies import models
from django.core.files.base import ContentFile


class MovieFactory(DjangoModelFactory):
    class Meta:
        model = models.Movie

    title = factory.Faker("name")
    description = factory.Faker("catch_phrase")
    stock = factory.Faker("pyint", min_value=1, max_value=50)
    sale_price = factory.Faker("pyint", min_value=100, max_value=500)
    rental_price = factory.Faker("pyint", min_value=20, max_value=50)
    poster = factory.LazyAttribute(
        lambda _: ContentFile(
            factory.django.ImageField()._make_data(
                {
                    "width": 1024,
                    "height": 768,
                }
            ),
            "example.jpg",
        )
    )
