#!/bin/sh
set -e

until psql $DATABASE_URL -c '\l'; do
    echo "Postgres is unavailable - sleeping"
    sleep 1
done

echo "Postgres is up - continuing"

python manage.py flush --no-input
python manage.py migrate

exec "$@"