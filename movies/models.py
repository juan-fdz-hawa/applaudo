from django.db import models
from django.core import validators
from django.contrib.auth import get_user_model
from pathlib import Path


def get_poster_filename(i, f):
    return f"movies/{i.title}{Path(f).suffix}"


class Movie(models.Model):
    title = models.CharField(
        max_length=250,
        db_index=True,
    )
    description = models.TextField()
    stock = models.PositiveIntegerField()
    rental_price = models.DecimalField(
        decimal_places=2,
        max_digits=8,
    )
    sale_price = models.DecimalField(
        decimal_places=2,
        max_digits=8,
    )
    available = models.BooleanField(
        default=True,
        db_index=True,
    )
    poster = models.ImageField(
        upload_to=get_poster_filename,
    )

    created = models.DateTimeField(
        auto_now_add=True,
    )
    updated = models.DateTimeField(
        auto_now=True,
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["title"],
                name="unique_title",
            )
        ]

    def __str__(self):
        return self.title


class Like(models.Model):
    movie = models.ForeignKey(
        Movie,
        on_delete=models.CASCADE,
        related_name="likes",
    )
    user = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        related_name="likes",
    )
    created = models.DateTimeField(
        auto_now_add=True,
    )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["movie", "user"],
                name="unique_like",
            )
        ]


class Rental(models.Model):
    movie = models.ForeignKey(
        Movie,
        on_delete=models.CASCADE,
        related_name="rentals",
    )
    rented_by = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        related_name="rentals",
    )
    copies = models.PositiveIntegerField(
        validators=[validators.MinValueValidator(1)],
    )
    due_at = models.DateField()
    returned_at = models.DateField(
        null=True,
    )
    created = models.DateTimeField(
        auto_now_add=True,
    )

    def fees(self):
        if self.returned_at:
            delta = (self.returned_at - self.due_at).days
            return delta * self.movie.rental_price * 1.25 if delta > 0 else 0
        return 0


class Sale(models.Model):
    movie = models.ForeignKey(
        Movie,
        on_delete=models.CASCADE,
        related_name="sales",
    )
    bought_by = models.ForeignKey(
        get_user_model(),
        on_delete=models.CASCADE,
        related_name="purchases",
    )
    copies = models.PositiveIntegerField(
        null=False,
        validators=[validators.MinValueValidator(1)],
    )
    created = models.DateTimeField(auto_now_add=True)
