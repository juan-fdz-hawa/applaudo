from django.test import TestCase
from movies.models import Movie, Rental, Sale
from django.contrib.auth import get_user_model
from datetime import datetime, timedelta


class MoviesTests(TestCase):
    def test_model_is_setup_correctly(self):
        Movie.objects.create(
            title="The Return of something",
            stock=1,
            rental_price=100,
            sale_price=1000,
        )

        Movie.objects.get(
            title="The Return of something",
        )


class RentalTests(TestCase):
    def test_model_is_setup_correctly(self):
        movie = Movie.objects.create(
            title="The Return of something",
            stock=1,
            rental_price=100,
            sale_price=1000,
        )

        jonny = get_user_model().objects.create_user(
            username="test",
            password="123",
        )

        Rental.objects.create(
            movie=movie, rented_by=jonny, copies=1, due_at=datetime.utcnow()
        )
        Rental.objects.get(
            pk=1,
        )


class SaleTests(TestCase):
    def test_model_is_setup_correctly(self):
        movie = Movie.objects.create(
            title="The Return of something",
            stock=1,
            rental_price=100,
            sale_price=1000,
        )

        jonny = get_user_model().objects.create_user(
            username="test",
            password="123",
        )

        Sale.objects.create(
            movie=movie,
            bought_by=jonny,
            copies=1,
        )
        Sale.objects.get(
            pk=1,
        )

    def test_fees_returned_before_due_date(self):
        movie = Movie(
            rental_price=100,
        )
        rental = Rental(
            movie=movie,
            due_at=datetime.utcnow() + timedelta(days=1),
            returned_at=datetime.utcnow(),
        )
        self.assertAlmostEqual(rental.fees(), 0)

    def test_fees_returned_after_due_date(self):
        movie = Movie(
            rental_price=100,
        )
        rental = Rental(
            movie=movie,
            due_at=datetime.utcnow(),
            returned_at=datetime.utcnow() + timedelta(days=2),
        )
        self.assertAlmostEqual(rental.fees(), 2 * 100 * 1.25)
