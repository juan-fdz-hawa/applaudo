from rest_framework.generics import get_object_or_404
from rest_framework import serializers
from django.db import transaction

from .models import Movie


class WithMovieAndUserPropsMixin:
    @property
    def movie(self):
        return self.context.get("movie")

    @property
    def user(self):
        return self.context.get("request").user


class InjectMovieIntoContextMixin:
    def get_serializer_context(self):
        context = super().get_serializer_context()

        movie_id = self.kwargs.get("parent_lookup_movie")
        if self.request and movie_id:
            context["movie"] = get_object_or_404(Movie, pk=movie_id)

        return context


class MovieTransactionMixin(WithMovieAndUserPropsMixin):
    def validate(self, attrs):
        if not self.movie.available:
            raise serializers.ValidationError("Movie is not available")
        return super().validate(attrs)

    def validate_copies(self, value):
        if value <= 0:
            raise serializers.ValidationError("Invalid number of copies")

        if value > self.movie.stock:
            raise serializers.ValidationError("Not enough stock")

        return value

    def save(self, **kwargs):
        with transaction.atomic():
            tx = super().save(**kwargs)
            tx.movie.stock -= tx.copies
            tx.movie.save()
            return tx
