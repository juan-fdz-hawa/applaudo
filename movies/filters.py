from rest_framework import filters
from django.db.models import Count


class MoviesOrderingFilter(filters.OrderingFilter):
    def filter_queryset(self, request, queryset, view):
        q = request.query_params.get("sort")
        if q in ["popularity", "-popularity"]:
            return queryset.annotate(popularity=Count("likes")).order_by(q)

        return super().filter_queryset(request, queryset, view)
