from django.test import TestCase
from django.contrib.auth import get_user_model

from movies.models import Movie

from auditlog.models import AuditLog


class AuditLogManagerTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        Movie.objects.create(
            title="The Return of something",
            description="Some description",
            stock=1,
            rental_price=100,
            sale_price=1000,
        )
        get_user_model().objects.create_user(username="test", password="123")

    def test_no_logs_created_if_nontracked_field_modified(self):
        movie = Movie.objects.get(pk=1)
        user = get_user_model().objects.get(username="test")
        updates = {"description": "No description"}

        result = AuditLog.objects.track_movie_changes(movie, updates, user)

        self.assertEqual(AuditLog.objects.all().count(), 0)
        self.assertEqual(len(result), 0)

    def test_no_logs_created_if_tracked_field_modified(self):
        movie = Movie.objects.get(pk=1)
        user = get_user_model().objects.get(username="test")

        updates = {
            "title": "The return of the killer whale",
            "rental_price": 100,
            "sale_price": 1_000_000,
        }

        AuditLog.objects.track_movie_changes(movie, updates, user)
        self.assertEqual(AuditLog.objects.all().count(), 2)

        for log in list(AuditLog.objects.filter(entity_name="movie")):
            if log.entity_field == "title":
                self.assertEqual(
                    log.entity_field_old_value,
                    "The Return of something",
                )
                self.assertEqual(
                    log.entity_field_new_value,
                    "The return of the killer whale",
                )
            else:
                self.assertEqual(log.entity_field_old_value, "1000.00")

                self.assertEqual(log.entity_field_new_value, "1000000")

            self.assertEqual(log.modified_by_id, user.id)
