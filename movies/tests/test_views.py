from datetime import date, timedelta

from django.test import TestCase
from django.urls import reverse
from django.contrib.auth import get_user_model


from rest_framework import status
from rest_framework.test import APIClient

from movies.models import Movie, Like
from auditlog.models import AuditLog

import factory
from factories import MovieFactory


class MovieTransactionViewSetTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        MovieFactory.create()
        get_user_model().objects.create_user(username="test", password="123")


class MovieViewSetTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        MovieFactory.create(available=True)
        MovieFactory.create(available=False)
        get_user_model().objects.create_user(username="test", password="123")
        get_user_model().objects.create_user(
            username="staff",
            password="123",
            is_staff=True,
        )


class RentingMoviesTests(MovieTransactionViewSetTests):
    def test_trying_to_rent_more_than_what_we_have_in_inventory(self):
        movie = Movie.objects.first()

        client = APIClient()
        client.login(username="test", password="123")

        resp = client.post(
            reverse("movie-rental-list", args=[movie.pk]),
            {
                "copies": movie.stock + 1,
                "due_at": date.today() + timedelta(days=1),
            },
            format="json",
        )

        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)

    def test_trying_to_rent_with_due_date_in_the_past(self):
        movie = Movie.objects.first()

        client = APIClient()
        client.login(username="test", password="123")

        resp = client.post(
            reverse("movie-rental-list", args=[movie.pk]),
            {
                "copies": movie.stock - 1,
                "due_at": date.today() + timedelta(days=-1),
            },
            format="json",
        )

        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)

    def test_making_a_rental(self):
        movie = Movie.objects.first()

        client = APIClient()
        client.login(username="test", password="123")

        resp = client.post(
            reverse("movie-rental-list", args=[movie.pk]),
            {
                "copies": movie.stock - 1,
                "due_at": date.today() + timedelta(days=2),
            },
            format="json",
        )

        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)

        movie.refresh_from_db()
        self.assertEqual(movie.stock, 1)


class SellingMoviesTests(MovieTransactionViewSetTests):
    def test_trying_to_sale_more_than_what_we_have_in_inventory(self):
        movie = Movie.objects.first()

        client = APIClient()
        client.login(username="test", password="123")

        resp = client.post(
            reverse("movie-sale-list", args=[movie.pk]),
            {"copies": movie.stock + 1},
            format="json",
        )

        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)

    def test_making_a_sale(self):
        movie = Movie.objects.first()

        client = APIClient()
        client.login(username="test", password="123")

        resp = client.post(
            reverse("movie-sale-list", args=[movie.pk]),
            {"copies": movie.stock - 1},
            format="json",
        )

        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)

        movie.refresh_from_db()
        self.assertEqual(movie.stock, 1)


class OrderingListOfMoviesTests(MovieViewSetTests):
    def test_users_are_able_to_sort_by_populatiry(self):
        user_1 = get_user_model().objects.first()
        user_2 = get_user_model().objects.last()

        popular = MovieFactory.create()
        popular.likes.create(user=user_1)

        most_popular = MovieFactory.create()
        most_popular.likes.create(user=user_1)
        most_popular.likes.create(user=user_2)

        client = APIClient()
        resp = client.get(reverse("movie-list"), {"sort": "-popularity"})

        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        self.assertEqual(resp.data["results"][0]["title"], most_popular.title)
        self.assertEqual(resp.data["results"][1]["title"], popular.title)


class FilteringListOfMoviesTests(MovieViewSetTests):
    def test_admins_can_filter_by_movie_availability(self):
        available_movies = list(Movie.objects.filter(available=True))
        unavailable_movies = list(Movie.objects.filter(available=False))

        client = APIClient()
        client.login(username="staff", password="123")

        resp = client.get(reverse("movie-list"), {"available": True})
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp.data["results"]), len(available_movies))
        for m in available_movies:
            self.assertIn(
                m.title,
                [r["title"] for r in resp.data["results"]],
            )

        resp = client.get(reverse("movie-list"), {"available": False})
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp.data["results"]), len(unavailable_movies))
        for m in unavailable_movies:
            self.assertIn(
                m.title,
                [r["title"] for r in resp.data["results"]],
            )


class ListingMoviesTests(MovieViewSetTests):
    def test_any_user_can_list_available_movies(self):
        available_movies = Movie.objects.filter(available=True)

        client = APIClient()
        resp = client.get(reverse("movie-list"))

        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp.data["results"]), len(available_movies))

    def test_any_user_can_view_an_available_movie(self):
        movie = Movie.objects.filter(available=True).first()

        client = APIClient()
        resp = client.get(reverse("movie-detail", args=[movie.pk]))

        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(resp.data["title"], movie.title)

    def test_users_can_not_see_unavailable_movies(self):
        unavailable_movie = Movie.objects.filter(available=False).first()

        client = APIClient()
        resp = client.get(
            reverse("movie-detail", args=[unavailable_movie.pk]),
        )

        self.assertEqual(resp.status_code, status.HTTP_404_NOT_FOUND)

    def test_admins_can_list_available_and_unavailable_movies(self):
        all_movies = Movie.objects.all()

        client = APIClient()
        client.login(username="staff", password="123")

        resp = client.get(reverse("movie-list"))

        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(len(resp.data["results"]), len(all_movies))

    def test_admins_can_see_unavailable_movies(self):
        unavailable_movie = Movie.objects.filter(available=False).first()

        client = APIClient()
        client.login(username="staff", password="123")

        resp = client.get(
            reverse("movie-detail", args=[unavailable_movie.pk]),
        )

        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(resp.data["title"], unavailable_movie.title)


class CreatingMoviesTests(MovieViewSetTests):
    def test_staff_can_create_movies(self):
        new_movie = factory.build(dict, FACTORY_CLASS=MovieFactory)

        client = APIClient()
        client.login(username="staff", password="123")

        resp = client.post(
            reverse("movie-list"),
            new_movie,
            format="multipart",
        )

        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        Movie.objects.get(title=new_movie["title"])

    def test_users_cannot_create_movies(self):
        new_movie = factory.build(dict, FACTORY_CLASS=MovieFactory)

        client = APIClient()
        client.login(username="test", password="123")

        resp = client.post(
            reverse("movie-list"),
            new_movie,
            format="multipart",
        )
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)


class UpdatingMoviesTests(MovieViewSetTests):
    def test_updating_a_movie_creates_auditlogs(self):
        client = APIClient()
        client.login(username="staff", password="123")

        resp = client.patch(
            reverse("movie-detail", args=[1]),
            {"title": "new title"},
            format="json",
        )

        self.assertEqual(resp.status_code, status.HTTP_200_OK)

        self.assertEqual(Movie.objects.get(pk=1).title, "new title")
        self.assertNotEqual(AuditLog.objects.all().count(), 0)

    def test_users_cannot_update_movies(self):
        new_movie = factory.build(dict, FACTORY_CLASS=MovieFactory)

        client = APIClient()
        client.login(username="test", password="123")

        resp = client.put(
            reverse("movie-detail", args=[1]),
            new_movie,
            format="multipart",
        )

        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)


class DeletingMoviesTests(MovieViewSetTests):
    def test_staff_can_delete_movies(self):
        client = APIClient()
        client.login(username="staff", password="123")
        resp = client.delete(reverse("movie-detail", args=[1]))
        self.assertEqual(resp.status_code, status.HTTP_204_NO_CONTENT)

    def test_users_cannot_delete_movies(self):
        client = APIClient()
        client.login(username="test", password="123")
        resp = client.delete(reverse("movie-detail", args=[1]))
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)


class MovieLikesTests(MovieViewSetTests):
    def test_annon_users_can_not_like_movies(self):
        client = APIClient()
        resp = client.post(reverse("movie-like-list", args=[1]))
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)

    def test_logged_users_can_like_movies(self):
        client = APIClient()
        client.login(username="test", password="123")

        resp = client.post(reverse("movie-like-list", args=[1]))

        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Like.objects.filter(movie_id=1).count(), 1)

    def test_logged_users_can_only_like_a_movie_once(self):
        movie = Movie.objects.get(pk=1)
        user = get_user_model().objects.get(username="test")
        movie.likes.create(user=user, movie=movie)

        client = APIClient()
        client.login(username="test", password="123")

        resp = client.post(reverse("movie-like-list", args=[1]))
        self.assertEqual(resp.status_code, status.HTTP_409_CONFLICT)


class MovieAvailabilityTests(MovieViewSetTests):
    def test_annon_users_can_not_modify_movie_availability(self):
        client = APIClient()
        resp = client.patch(
            reverse("movie-detail", args=[1]),
            {"available": True},
        )
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)

    def test_normal_users_can_not_modify_movie_availability(self):
        client = APIClient()
        client.login(username="test", password="123")
        resp = client.patch(
            reverse("movie-detail", args=[1]),
            {"available": True},
        )
        self.assertEqual(resp.status_code, status.HTTP_403_FORBIDDEN)

    def test_admin_users_can_make_a_movie_available(self):
        movie = Movie.objects.get(pk=1)
        movie.available = False
        movie.save()

        client = APIClient()
        client.login(username="staff", password="123")
        resp = client.patch(
            reverse("movie-detail", args=[1]),
            {"available": True},
        )

        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        movie.refresh_from_db()
        self.assertEqual(movie.available, True)
