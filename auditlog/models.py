from django.db import models

from .managers import AuditLogManager


class AuditLog(models.Model):
    entity_name = models.CharField(max_length=250)
    entity_id = models.PositiveIntegerField()

    modified_by_id = models.PositiveIntegerField()

    entity_field = models.CharField(max_length=250)
    entity_field_old_value = models.TextField()
    entity_field_new_value = models.TextField()

    objects = AuditLogManager()
    created = models.DateTimeField(auto_now_add=True)
