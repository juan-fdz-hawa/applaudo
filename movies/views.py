from datetime import date
from django.db.utils import IntegrityError
from django.db import transaction
from django.utils.decorators import method_decorator

from rest_framework import viewsets, status, mixins, parsers
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import filters
from rest_framework_extensions.mixins import NestedViewSetMixin
from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg.utils import swagger_auto_schema

from auditlog.models import AuditLog

from .models import Movie, Sale, Rental, Like
from .serializers import (
    MovieSerializer,
    SaleSerializer,
    RentalSerializer,
    LikeSerializer,
)
from .permissions import IsStaffOrReadOnly
from .filters import MoviesOrderingFilter
from .mixins import InjectMovieIntoContextMixin


class MovieViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    parser_classes = [
        parsers.MultiPartParser,
        parsers.FormParser,
        parsers.JSONParser,
    ]
    permission_classes = [IsStaffOrReadOnly]
    serializer_class = MovieSerializer
    queryset = Movie.objects.all()

    filter_backends = [
        DjangoFilterBackend,
        filters.SearchFilter,
        MoviesOrderingFilter,
    ]
    filterset_fields = ["available"]

    search_fields = ["title"]

    ordering_fields = ["title", "popularity"]
    ordering = ["title"]

    def get_queryset(self):
        if self.request is None:
            return Movie.objects.none()

        q = super().get_queryset()
        u = self.request.user
        return q if u.is_staff else q.filter(available=True)

    def update(self, request, *args, **kwargs):
        movie = self.get_object()

        serializer = self.get_serializer(
            movie,
            data=request.data,
            partial=kwargs.pop("partial", False),
        )
        serializer.is_valid(raise_exception=True)

        with transaction.atomic():
            AuditLog.objects.track_movie_changes(
                movie,
                serializer.validated_data,
                request.user,
            )
            self.perform_update(serializer)

        return Response(serializer.data)


class LikeViewSet(
    InjectMovieIntoContextMixin,
    NestedViewSetMixin,
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = [IsAuthenticated]
    serializer_class = LikeSerializer
    queryset = Like.objects.all().prefetch_related("movie", "user")

    def create(self, request, *args, **kwargs):
        try:
            return super().create(request, *args, **kwargs)
        except IntegrityError:
            return Response(status=status.HTTP_409_CONFLICT)


@method_decorator(
    name="create",
    decorator=swagger_auto_schema(query_serializer=RentalSerializer),
)
class RentalViewSet(
    InjectMovieIntoContextMixin,
    NestedViewSetMixin,
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = [IsAuthenticated]
    serializer_class = RentalSerializer
    queryset = Rental.objects.all().prefetch_related("movie", "rented_by")

    @action(
        methods=["put"],
        detail=True,
    )
    def return_movie(self, request, pk=None):
        rental = self.get_object()
        rental.returned_at = date.today()
        rental.save()

        return Response(
            status=status.HTTP_200_OK,
            data={"fees": rental.fees()},
        )


@method_decorator(
    name="create",
    decorator=swagger_auto_schema(query_serializer=SaleSerializer),
)
class SaleViewSet(
    InjectMovieIntoContextMixin,
    NestedViewSetMixin,
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    viewsets.GenericViewSet,
):
    permission_classes = [IsAuthenticated]
    serializer_class = SaleSerializer
    queryset = Sale.objects.all().prefetch_related("movie", "bought_by")
