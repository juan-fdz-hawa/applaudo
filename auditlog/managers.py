from django.db import models


class AuditLogManager(models.Manager):
    def track_movie_changes(self, movie, updates, user):
        fields = {"title", "rental_price", "sale_price"}

        logs = [
            self._map_log(movie, updates, user, f)
            for f in updates.keys()
            if f in fields and getattr(movie, f) != updates.get(f)
        ]

        return self.bulk_create(logs)

    def _map_log(self, movie, updates, user, f):
        return self.model(
            entity_name="movie",
            entity_id=movie.pk,
            modified_by_id=user.pk,
            entity_field=f,
            entity_field_old_value=str(getattr(movie, f)),
            entity_field_new_value=str(updates.get(f)),
        )
