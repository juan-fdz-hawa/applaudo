## Installation

Make sure you have docker and docker-compose installed and working then just run `docker-compose up` at the root of the project. You should be able to access the API docs via:
`http://127.0.0.1:8000/api/v1/redoc/`.

You will need to create an admin/staff account to test some of the endpoints that mutate movies, for that just run `docker-compose exec web python manage.py createsuperuser` and follow the instructions on screen.