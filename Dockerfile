FROM python:3.9.7-alpine

WORKDIR /usr/src/app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# PG dependencies
RUN apk update && apk add postgresql-dev gcc python3-dev musl-dev postgresql-client
# Pillow dependencies
RUN apk add gcc python3-dev jpeg-dev zlib-dev

# Auth dependecies
RUN apk add libffi-dev

RUN pip install --upgrade pip
COPY ./requirements.txt .
RUN pip install -r requirements.txt

COPY ./entrypoint.sh .
RUN chmod +x /usr/src/app/entrypoint.sh

COPY . .

ENTRYPOINT ["/usr/src/app/entrypoint.sh"]