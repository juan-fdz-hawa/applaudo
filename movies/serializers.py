from datetime import date

from rest_framework import serializers
from rest_framework_extensions.serializers import PartialUpdateSerializerMixin

from .models import Movie, Sale, Rental, Like
from .mixins import WithMovieAndUserPropsMixin, MovieTransactionMixin


class MovieSerializer(
    PartialUpdateSerializerMixin,
    serializers.ModelSerializer,
):
    class Meta:
        model = Movie
        fields = [
            "id",
            "title",
            "description",
            "stock",
            "rental_price",
            "sale_price",
            "created",
            "updated",
            "poster",
            "available",
        ]
        read_only_fields = ["id", "created", "updated"]


class LikeSerializer(
    serializers.ModelSerializer,
    WithMovieAndUserPropsMixin,
):
    class Meta:
        model = Like
        fields = ["id", "movie", "user", "created"]
        read_only_fields = ["id", "movie", "user", "created"]

    def create(self, validated_data):
        validated_data["movie"] = self.movie
        validated_data["user"] = self.user
        return super().create(validated_data)


class RentalSerializer(
    MovieTransactionMixin,
    PartialUpdateSerializerMixin,
    serializers.ModelSerializer,
):
    class Meta:
        model = Rental
        fields = [
            "id",
            "movie",
            "rented_by",
            "copies",
            "due_at",
            "returned_at",
            "created",
        ]
        read_only_fields = [
            "id",
            "movie",
            "rented_by",
            "returned_at",
            "created",
        ]

    def validate_due_at(self, value):
        if value <= date.today():
            raise serializers.ValidationError("Due date can't be in the past")

        return value

    def create(self, validated_data):
        validated_data["movie"] = self.movie
        validated_data["rented_by"] = self.user
        return super().create(validated_data)


class SaleSerializer(
    MovieTransactionMixin,
    PartialUpdateSerializerMixin,
    serializers.ModelSerializer,
):
    class Meta:
        model = Sale
        fields = [
            "id",
            "movie",
            "bought_by",
            "copies",
            "created",
        ]
        read_only_fields = [
            "id",
            "movie",
            "bought_by",
            "created",
        ]

    def create(self, validated_data):
        validated_data["movie"] = self.movie
        validated_data["bought_by"] = self.user
        return super().create(validated_data)
